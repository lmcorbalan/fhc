module.exports = sides =>  {
  if (isTriangle(sides)) {
    return 'triangles';
  } else if (isRectangle(sides)) {
    return 'rectangles';
  } else if (isSquare(sides)) {
    return 'squares';
  } else {
    return 'else';
  }
}

function isTriangle(sides) {
  if (sides.length !== 3) {
    return false;
  }

  const [a,b,c] = sides;
  const ab = a + b;
  const bc = b + b;
  const ca = c + a;

  return (ab > c && bc > a && ca > a);
}

function isRectangle(sides) {
  if (sides.length !== 4) {
    return false;
  }

  const orderedSides = sides.slice(0).sort((a,b) => a - b);

  return (orderedSides[0] === orderedSides[2] && orderedSides[1] === orderedSides[4]);
}

function isSquare(sides) {
  if (sides.length !== 4) {
    return false;
  }

  const sample = sides[0];
  return sides.every((side) => side === sample);
}
