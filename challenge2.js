/*
2) Write a function to calculate the cumulative TTL of the following set of
requests. (The provided answer is correct and should not be modified. )

const requests = [
{requestId: 'poiax',  startedAt: 1489744808, ttl: 8},
{requestId: 'kdfhd',  startedAt: 1489744803, ttl: 3},
{requestId: 'uqwyet', startedAt: 1489744806, ttl: 12},
{requestId: 'qewaz',  startedAt: 1489744810, ttl: 1}
]

const cumulativeTtl = 15
*/

const requests = [
  {requestId: 'poiax',  startedAt: 1489744808, ttl: 8},
  {requestId: 'kdfhd',  startedAt: 1489744803, ttl: 3},
  {requestId: 'uqwyet', startedAt: 1489744806, ttl: 12},
  {requestId: 'qewaz',  startedAt: 1489744810, ttl: 1}
];

function cumulativeTtl(requests) {
  const start = requests
    .map((req) => req.startedAt)
    .sort((a, b) => a - b);

  const end = requests
    .map((req) => req.startedAt + req.ttl)
    .sort((a, b) => b - a);

  return end[0] - start[0];
}

console.log(cumulativeTtl(requests));
