/*
1) Write a function to transform array startingArray to array resultingArray and
print the elements of the resulting array to the console.

var originalArray = [2, 4, 6, 8, 9, 15];
var finalArray = ["4", "16", "64"]
*/

const originalArray = [2, 4, 6, 8, 9, 15]

const result = originalArray
  .filter((el) => el % 3 !== 0)
  .map((el) => el**2);

console.log(result);
