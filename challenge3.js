/*
3) Read a text file containing a set of flat polygons represented as one
polygon per line. Each line contains a comma-separated list of side lengths
(for example “3,4,8,5,7”). Write a function to classify the set of polygons
into four mutually exclusive subsets: triangles, rectangles, squares, and
everything else. The union of all four subsets should be the original set of
polygons. All the sides of the polygons are connected and the angles between
them are irrelevant. Only consider the lengths. Consider that the code will be
used in a high load environment.
*/

const classify = require('./classifyPolygons');
const readline = require('readline');
const fs = require('fs');
resolve = require('path').resolve;

const filePath = resolve('polygons.txt');

const rl = readline.createInterface({
  input: fs.createReadStream(filePath)
});

let classified = {
  triangles: [],
  rectangles: [],
  squares: [],
  else: []
};

rl.on('line', function (line) {
  const sides = line.split(',');
  const type = classify(sides);
  classified[type].push(sides);
}).on('close', () => {
  console.log(classified);
});
